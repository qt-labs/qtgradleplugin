# Qt Gradle Plugin

## Plugin as dependency

All of the "blocks" mentioned below might already exist.
If that is the case, no need to create a new one.

### Modify settings.gradle (.kts)

Open settings.gradle (.kts) file.
Within the `pluginManagement` block, add `mavenCentral` and `maven` repositories.

### Groovy/Kotlin

```
pluginManagement {
    repositories {
        mavenCentral()
    }
}
```

### Modify build.gradle (.kts)

Open `build.gradle` (.kts) file. Inside the plugins block, include the plugin ID and version.

### Groovy

```Groovy
plugins {
    id 'org.qtproject.qt.gradleplugin' version '1.+'
}
```

### Kotlin

```Groovy
plugins {
    id("org.qtproject.qt.gradleplugin") version("1.+")
}
```

Define `QtBuild` block to specify project and Qt configurations.

### Groovy

```Groovy
QtBuild {
    // Qt installation directory
    qtPath file('foobar/Qt/6.8.0')

    // QML project directory
    projectPath file('../qmlapp')

    // Optional: Qt kit directory location
    // If defined build will be done to only that ABI
    // else will build using Multi-ABI
    qtKitDir file('home/<your-username>/<your-Qt-builds>/<your-Qt-kit>')

    // Optional: Extra cmake arguments for configuration
    extraCMakeArguments = ['-DCMAKE_BUILD_TYPE=Release']

    // Optional: If your ninja is installed somewhere other than Qt/Tools/Ninja or
    // not found in system path you can define it here
    ninjaPath 'your/path/to/Ninja'
}
```

### Kotlin

```Groovy
QtBuild {
    // Qt installation directory
    qtPath = file("foobar/Qt/6.8.0")

    // QML project directory
    projectPath = file("../qmlapp")

    // Optional: Qt kit directory location
    // If defined build will be done to only that ABI
    // else will build using Multi-ABI
    qtKitDir = file('home/<your-username>/<your-Qt-builds>/<your-Qt-kit>')

    // Optional: Extra cmake arguments for configuration
    setExtraCMakeArguments("-DCMAKE_BUILD_TYPE=Release")

    // Optional: If your ninja is installed somewhere other than Qt/Tools/Ninja or
    // not found in system path you can define it here
    ninjaPath = 'your/path/to/Ninja'
}
```

## Build plugin

Run the following command to build and run tests for the plugin

```bash
./gradlew build
```

The .jar file can be found under folder `build/libs` and the test results can be found
under the folder `build/reports/tests`.

## Publish plugin to Local Maven

Run the following command to build and publish the plugin in local Maven:

```bash
./gradlew publishToMavenLocal
```
## Publish plugin to Maven

Run the following command to publish the plugin into a Maven repository that is defined by the
project level `./gradle.properties` or the global level `~/.gradle/gradle.properties` file.
Required properties are `publishingUrl`, `publishingUser` and `publishingPass`.

Gradle properties can also be passed by using the command-line options `-P`.

```bash
./gradlew publish

// or passing publishing parameters via command-line
./gradlew publish -PpublishingUrl=http://example.com/maven -PpublishingUser=user -PpublishingPass=pass
```
## Publish plugin to Maven Central

Run the following command to publish the plugin into a Maven repository that is defined by the
global level `~/.gradle/gradle.properties` file. Required properties are `publishingUser` and
`publishingPass`. These properties should contain the token username and password generated in
Maven Central.

```bash
./gradlew publishAllPublicationsToCentralPortal
```

Navigate to your Qt Quick as Android View project.

### Modify settings.gradle (.kts)

Add Plugin Path: Open `settings.gradle` (.kts) file.
Within the pluginManagement block, add `mavenLocal` repository.

### Groovy & Kotlin
```Groovy
pluginManagement {
    repositories {
        mavenLocal()
    }
}
```

For the [build.gradle (.kts)](#modify-buildgradle-kts)


## Running from Android Studio IDE

`QtBuildTask` will automatically run when android application is built.

## Running from terminal

You can now run QtBuildTask with:

```bash
./gradlew QtBuildTask
```

## Debugging

Confirm that plugin is successfully added by running

```bash
./gradlew tasks --group QtProject
```

Output should look like this:

> Task :tasks
>
> ------------------------------------------------------------
> Tasks runnable from root project 'My Application'
> ------------------------------------------------------------
>
> QtProject tasks
> ---------------
> QtBuildTask - Qt Project AAR Build
>
> To see all tasks and more detail, run gradlew tasks --all
