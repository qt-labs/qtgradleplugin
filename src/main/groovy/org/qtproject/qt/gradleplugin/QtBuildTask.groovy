// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.gradleplugin

import groovy.json.JsonSlurper
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

class QtBuildTask extends DefaultTask {
    @Input
    String qtPath
    @Optional @Input
    String qtKitDir
    @InputDirectory
    File projectPath
    @Optional @Input
    String qtNinjaPath
    @Optional @Input
    String[] extraCMakeArguments
    @OutputDirectory
    File buildDirectory

    private final static String exeSuffix = execSuffix()

    QtBuildTask() {
        group = 'QtProject'
        description = 'Qt Project AAR Build'
    }

    @TaskAction
    def build() {
        exec configCommand()
        createEmptySettingsGradle()
        exec buildCommand()
        addDependency()
    }

    def addDependency() {
        def targetName = Utils.getAndroidBuildTargetName(buildDirectory)
        project.dependencies.add('implementation',
                project.fileTree(dir: "${buildDirectory}/android-build-${targetName}",
                        include: ["${targetName}.aar"]))
    }

    @Internal
    def getQtCMakeWrapperPath() {
        def qtCMakeWrapperPath = "$qtKitDir/bin/qt-cmake"
        if (isWindows())
            qtCMakeWrapperPath += '.bat'
        return qtCMakeWrapperPath
    }

    def resolveNinjaPath() {
        // Check if 'ninja' is defined in build.gradle
        if (qtNinjaPath) {
            qtNinjaPath = new File("$qtNinjaPath/ninja${exeSuffix}").absolutePath
            return
        }

        // Search for 'ninja' under PATH directories
        qtNinjaPath = findNinjaFromEnvPath()
        if (qtNinjaPath && new File(qtNinjaPath).exists())
            return

        // Check 'ninja' under Default Qt install directory
        def qtParentPath = new File(qtPath).parent
        qtNinjaPath = new File("$qtParentPath/Tools/Ninja/ninja$exeSuffix").absolutePath
        if (new File(qtNinjaPath).exists())
            return

        def ninjaNotFoundError = 'No valid path to Ninja found. Checked locations:\n' +
                '    - Manually specified path in build.gradle QtBuild extension:' +
                ' ninjaPath = not set\n' +
                '    - Environment PATH\n' +
                '    - Default Qt installation directory: ~/Qt/Tools/Ninja'

        Utils.logAndThrowException(ninjaNotFoundError.trim())
    }

    static def findNinjaFromEnvPath() {
        def envPathFolders = System.getenv('PATH')
        for (def folder in envPathFolders.split(File.pathSeparator)) {
            for (File file in new File(folder).listFiles()) {
                if (file.name == "ninja$exeSuffix")
                    return file.absolutePath
            }
        }
    }

    static def execSuffix() {
        return "${isWindows() ? '.exe' : ''}"
    }

    static def isWindows() {
        return System.properties['os.name'].contains('Windows')
    }

    def createEmptySettingsGradle() {
        def targetName = Utils.getAndroidBuildTargetName(buildDirectory)
        def settingsGradle = new File("${buildDirectory}/android-build-${targetName}/settings.gradle")
        settingsGradle.parentFile.mkdirs()
        settingsGradle.createNewFile()
        return settingsGradle
    }

    def resolvedSdkDirectory() {
        def sdkLocation
        def localPropertiesFile = project.rootProject.file('local.properties')

        if (localPropertiesFile.exists()) {
            def properties = new Properties()
            localPropertiesFile.withInputStream { inputStream ->
                properties.load(inputStream)
            }
            sdkLocation = properties.getProperty('sdk.dir')
        } else {
            sdkLocation = System.getenv("ANDROID_SDK_ROOT")
        }

        if (!new File(sdkLocation).exists()) {
            Utils.logAndThrowException("Android SDK path not found." +
                    " Add the 'sdk.dir' property in local.properties file or" +
                    " the 'ANDROID_SDK_ROOT' environment variable.")
        }
        return sdkLocation
    }

    def extractNDKVersion(def qtCoreJson) {
        // Retrieve NDK version using 6.8 or 6.9 Core.json key format
        def jsonKey = qtCoreJson.built_with ? 'built_with.android.ndk.version' :
                'platforms.targets.ndk_version'
        return extractValueFromJson(qtCoreJson, jsonKey)
    }

    def getNDKDirectory(String sdkLocation, ndkVersion) {
        def ndkPath = "${sdkLocation}/ndk/$ndkVersion"
        if (!new File(ndkPath).exists())
            Utils.logAndThrowException("NDK Location not found at: $ndkPath.")
        return ndkPath
    }

    static def extractValueFromJson(jsonData, String jsonKey) {
        def keys = jsonKey.split("\\.")
        if (keys.size() == 1)
            return jsonData[keys[0]]
        def result = keys.inject(jsonData) { obj, key ->
            obj instanceof List ? obj.findResult { it[key] } : obj?[key]
        }
        return result
    }

    static def readJsonFromFile(String jsonFilePath) {
        def jsonSlurper = new JsonSlurper()
        def jsonData = new File(jsonFilePath).text
        return jsonSlurper.parseText(jsonData)
    }

    // @TODO: QTTA-297 Instead of taking the first ABI found take one that,
    // matches with the running device/emulator.
    def listAndroidABIsAndSelectFirst() {
        def firstMatch = new File(qtPath).listFiles().find { file ->
            file.name ==~ Utils.androidAbiDirNameFormat()
        }

        if (!firstMatch)
            Utils.logAndThrowException("No Qt for Android kit found from: $qtPath")
        return new File(firstMatch.path)
    }

    def resolveCoreJson() {
        def qtCoreJsonPath = "$qtKitDir/modules/Core.json"

        if (!new File(qtCoreJsonPath).exists())
            Utils.logAndThrowException("No Core.json file found under $qtCoreJsonPath.")
        return readJsonFromFile(qtCoreJsonPath)
    }

    def getCoreJsonPlatform(def qtCoreJson) {
        // Retrieve platform name using 6.8 or 6.9 Core.json key format
        def jsonKey = qtCoreJson.built_with ? 'built_with.target_system' :
                'platforms.name'
       return extractValueFromJson(qtCoreJson, jsonKey)
    }

    // Getting the ABI when not using multi-abi
    // by checking the architecture from the Core.json
    def getCoreJsonArchitecture(def qtCoreJson) {
        // Retrieve architecture using 6.8 or 6.9 Core.json key format
        def jsonKey = qtCoreJson.built_with ? 'built_with.architecture' :
                'platforms.targets.architecture'
        return extractValueFromJson(qtCoreJson, jsonKey)
    }

    def configCommand() {
        def buildMultiABI = !qtKitDir
        if (!qtKitDir)
            qtKitDir = listAndroidABIsAndSelectFirst()
        def qtCoreJSon = resolveCoreJson()

        def architecture = getCoreJsonArchitecture(qtCoreJSon)
        def currentABI = Utils.abiFromArchitecture(architecture)

        def platform = getCoreJsonPlatform(qtCoreJSon)
        if (platform.toString().toLowerCase() != 'android') {
            Utils.logAndThrowException(
                    "The configured kit in build.gradle(.kts) is not an Android kit.\n" +
                    "The kit's path: $qtKitDir")
        }

        def qtCMakeWrapperPath = getQtCMakeWrapperPath()

        resolveNinjaPath()

        def sdkDirectory = resolvedSdkDirectory()
        def ndkDirectory = getNDKDirectory(sdkDirectory, extractNDKVersion(qtCoreJSon))

        def cmd = [
            qtCMakeWrapperPath,
            '-S', projectPath,
            '-B', getBuildDirectory(),
            '-G', 'Ninja',
            '-DQT_ANDROID_GENERATE_JAVA_QTQUICKVIEW_CONTENTS=ON',
            '-DQT_USE_TARGET_ANDROID_BUILD_DIR=ON',
            "-DCMAKE_MAKE_PROGRAM=$qtNinjaPath",
            "-DANDROID_SDK_ROOT=$sdkDirectory",
            "-DANDROID_NDK_ROOT=$ndkDirectory",
            "-DANDROID_ABI=$currentABI",
            "-DQT_ANDROID_BUILD_ALL_ABIS=${buildMultiABI ? 'ON': 'OFF'}"
        ]

        return cmd + extraCMakeArguments.collect()
    }

    def buildCommand() {
         return [qtNinjaPath, '-C', getBuildDirectory(), 'aar']
    }

    def exec(ArrayList command) {
        def process
        def commandWithSpacesQuoted = command.collect {
            if (it.toString().contains(' ')) {
                return "\"$it\""
            } else {
                return it
            }
        }

        try {
            process = isWindows() ? commandWithSpacesQuoted.join(' ').execute() : command.execute()
            def logHandler = new LogStreamHandler(logger)

            logHandler.start(process)
            def retCode = process.waitFor()
            logHandler.stop()

            if (retCode != 0)
                Utils.logAndThrowException("the command '${commandWithSpacesQuoted.first()}'" +
                        " returned error code $retCode.\n" +
                        "> Command arguments: ${commandWithSpacesQuoted[1..-1]}")
        } catch (IOException | InterruptedException e) {
            Utils.logAndThrowException("executing command '${commandWithSpacesQuoted.first()}'\n" +
                    "> what happened: ${e.message}")
        } finally {
            if (process)
                process.destroy()
        }
    }
}
