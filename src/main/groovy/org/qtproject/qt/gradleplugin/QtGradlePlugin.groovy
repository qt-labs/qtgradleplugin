// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.gradleplugin

import org.gradle.api.Plugin
import org.gradle.api.Project

class QtPluginExtension {
    File qtPath
    File qtKitDir
    File projectPath
    String ninjaPath
    String[] extraCMakeArguments
}

class QtGradlePlugin implements Plugin<Project> {
    void apply(Project project) {
        def extension = project.extensions.create('QtBuild', QtPluginExtension)

        def buildTask = project.tasks.register('QtBuildTask', QtBuildTask) {
            qtPath = extension.qtPath
            qtKitDir = extension.qtKitDir
            projectPath = extension.projectPath
            qtNinjaPath = extension.ninjaPath
            extraCMakeArguments = extension.extraCMakeArguments
            buildDirectory = getBuildDirectory(project, projectPath?.name)
        }

        project.tasks.configureEach { task ->
            switch (task.name) {
                // Declaring tasks to depend on QtBuildTask output when building from:
                case ['assemble', 'assembleDebug', 'assembleRelease', // Android Studio
                      'collectReleaseDependencies', 'lintVitalAnalyzeRelease', // Groovy project CLI
                      'generateReleaseLintVitalReportModel']: // Kotlin project CLI
                    task.dependsOn(buildTask)
                    break
            }
        }

        project.afterEvaluate {
            if (buildTask.get().buildDirectory.exists())
                buildTask.get().addDependency()
        }
    }

    static File getBuildDirectory(project, qtProjectName) {
        def buildPath = project.getLayout().getBuildDirectory().getAsFile().get()
        return new File("${buildPath}/qt_generated/$qtProjectName")
    }
}
