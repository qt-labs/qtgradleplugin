// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.gradleplugin

import org.gradle.api.GradleException

@Singleton
class Utils {
    static String ANDROID_ABI_ARM64_V8A = "arm64-v8a"
    static String ANDROID_ABI_ARMEABI_V7A = "armeabi-v7a"
    static String ANDROID_ABI_X86_64 = "x86_64"
    static String ANDROID_ABI_X86 = "x86"

    static def abis = [
            'arm64': ANDROID_ABI_ARM64_V8A, 'arm': ANDROID_ABI_ARMEABI_V7A,
            'x86_64': ANDROID_ABI_X86_64, 'i386': ANDROID_ABI_X86
    ]

    static String androidAbiDirNameFormat() {
        return "android_($ANDROID_ABI_X86_64|$ANDROID_ABI_ARM64_V8A|" +
                "$ANDROID_ABI_ARMEABI_V7A|$ANDROID_ABI_X86)"
    }

    static String abiFromArchitecture(String architecture) {
        return abis.find { it.key == architecture }?.value
    }

    static def getAndroidBuildTargetName(File buildDir) {
        def deploymentSettingFile = buildDir.listFiles()
                .findAll { it.name.contains('-deployment-settings.json') }
                .max { it.lastModified() }

        if (!deploymentSettingFile)
            logAndThrowException("Deployment file not found in '$buildDir'")

        def pattern = ~/android-(.*?)-deployment-settings\.json/
        def matcher = deploymentSettingFile.name =~ pattern
        if (!matcher)
            logAndThrowException("File name does not match the expected pattern: ${deploymentSettingFile.name}")

        return matcher.group(1)
    }

    static logAndThrowException(String errorMessage) {
        throw new GradleException(errorMessage)
    }
}
