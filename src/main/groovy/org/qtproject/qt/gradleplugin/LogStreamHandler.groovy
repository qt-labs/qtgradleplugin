// Copyright (C) 2025 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.gradleplugin

import org.gradle.api.logging.Logger

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class LogStreamHandler {
    private Logger m_logger
    private ExecutorService m_executorService = Executors.newFixedThreadPool(2)

    LogStreamHandler(Logger logger) {
        m_logger = logger
    }

    void start(Process process) {
        m_executorService.submit {
            if (!process.isAlive())
                return
            processStream(process.inputStream, { String line ->
                // TODO: QTTA-144 Improve the logger to avoid parsing warnings unintentionally
                m_logger.lifecycle(line.trim().replaceFirst("Warning: ", "Warn: "))
            })
        }
        m_executorService.submit {
            if (!process.isAlive())
                return
            processStream(process.errorStream, { String line ->
                m_logger.error(line.trim())
            })
        }
    }

    void stop() {
        m_executorService.shutdownNow()
    }

    private void processStream(InputStream stream, Closure<String> handler) {
        try {
            stream.eachLine { String line ->
                handler(line)
            }
        } catch (Exception e) {
            m_logger.error("Error processing stream: ${e.message}", e)
        }
    }
}
