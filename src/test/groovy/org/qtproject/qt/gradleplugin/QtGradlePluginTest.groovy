// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.gradleplugin

import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class QtGradlePluginTest extends Specification {
    @Delegate
    QtGradlePluginTestHelper testHelper = new QtGradlePluginTestHelper()

    void setupSpec() {
        QtGradlePluginTestHelper.mockProjectAndSetExtensionProperties()
    }

    void 'test QtGradlePlugin creates QtPluginExtension'() {
        given: 'a mock project'
        def project = ProjectBuilder.builder().build()

        when: 'plugin is applied'
        project.plugins.apply('org.qtproject.qt.gradleplugin')

        then: 'extension is found by name'
        project.extensions.findByName('QtBuild') != null
    }

    void 'test QtGradlePlugin registers QtBuildTask'() {
        given: 'a mock project'
        def project = ProjectBuilder.builder().build()

        when: 'plugin is applied'
        project.plugins.apply('org.qtproject.qt.gradleplugin')

        then: 'QtBuildTask is found'
        project.tasks.named('QtBuildTask') != null
    }

    void 'test QtPluginExtension with valid directory path'() {
        given: 'extension is found by type'
        def pluginExtension = getQtPluginExtensionFromProject()

        when: 'extension is given a valid path'
        def validPath = 'src/test/groovy'
        pluginExtension.qtKitDir = new File(validPath)

        then: 'the qtKitDir in QtBuildTask is set correctly'
        def buildTask = getQtBuildTaskFromProject()
        new File(buildTask.qtKitDir).exists()
    }

    void 'test QtPluginExtension without valid directory path'() {
        given: 'extension is found'
        def pluginExtension = getQtPluginExtensionFromProject()

        when: 'extension is given an empty path'
        pluginExtension.qtPath = new File('')

        then: 'path in the extension is empty'
        def buildTask = getQtBuildTaskFromProject()
        !new File(buildTask.qtPath).exists()
    }

    void 'test QtGradlePlugin gets valid build directory path'() {
        given: 'qt project path is set to extension'
        def pluginExtension = getQtPluginExtensionFromProject()
        pluginExtension.projectPath = QtGradlePluginTestHelper.qtProjectPathMock

        when: 'getBuildDirectory is called'
        def projectName = pluginExtension.projectPath.getName()
        def buildDirectory = QtGradlePlugin.getBuildDirectory(
                QtGradlePluginTestHelper.mockProject, projectName)

        then: 'build directory ends with "qt_generated/projectName"'
        def pattern = ~/qt_generated[\\\/]${projectName}$/
        buildDirectory.path.find(pattern)
    }
}
