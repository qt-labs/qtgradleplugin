// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.gradleplugin

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder

class QtGradlePluginTestHelper {
    static File qtKitDirMock = new File('mock/qt/kit/dir')
    static File qtProjectPathMock = new File('MockProject')
    static File qtPathMock = new File('mock/qt/path')
    static Project mockProject

    String validMockJsonFile() {
        /{
            "built_with": {
                "android": {
                    "api_version": "Working!",
                    "ndk": {
                        "version": "1.2.3456789"
                    }
                },
                "compiler_id": "Clang"
            }
        }/
    }

    String invalidMockJsonFile() {
        /{
            "this is just a text file"
        }/
    }

    String coreJson_6_8() {
        /{
            "name": "Core",
            "repository": "qtbase",
            "version": "6.8.0",
            "built_with": {
                "android": {
                    "api_version": "android-34",
                    "ndk": {
                        "version": "26.1.10909125"
                    }
                },
                "compiler_id": "Clang",
                "compiler_target": "aarch64-none-linux-android28",
                "compiler_version": "17.0.2",
                "cross_compiled": true,
                "target_system": "Android",
                "architecture": "arm64"
            }
        }/
    }

    String coreJson_6_9() {
        /{
            "schema_version": 2,
            "name": "Core",
            "repository": "qtbase",
            "version": "6.9.0",
            "platforms": [
                {
                    "name": "Android",
                    "version": "1",
                    "compiler_id": "Clang",
                    "compiler_version": "17.0.2",
                    "targets": [
                        {
                            "api_version": "android-34",
                            "ndk_version": "26.1.10909125",
                            "architecture": "arm64",
                            "abi": "arm64-little_endian-lp64"
                        }
                    ]
                }
            ]
        }/
    }

    static Project mockProjectAndSetExtensionProperties() {
        mockProject = ProjectBuilder.builder().build()
        mockProject.plugins.apply('org.qtproject.qt.gradleplugin')
        def pluginExtension = mockProject.extensions.getByType(QtPluginExtension)
        pluginExtension.projectPath = qtProjectPathMock
        def buildDir = mockProject.layout.buildDirectory.asFile.get()
        pluginExtension.qtPath = new File("${buildDir}/${qtPathMock}")
        return mockProject
    }

    QtPluginExtension getQtPluginExtensionFromProject() {
        return mockProject.extensions.getByType(QtPluginExtension)
    }

    QtBuildTask getQtBuildTaskFromProject() {
        return mockProject.tasks.named('QtBuildTask', QtBuildTask).get()
    }

    File createMockFile(String path) {
        def mockFile = new File(path)
        mockFile.parentFile.mkdirs()
        mockFile.createNewFile()
        return mockFile
    }

    File setupMockJsonFile(String fileName, String jsonData) {
        def buildDir = mockProject.layout.buildDirectory.asFile.get()
        def jsonFile = createMockFile("${buildDir}/${fileName}")
        jsonFile.text = jsonData
        return jsonFile
    }

    File createDeploymentSettingsFileMock(File buildDir) {
        def targetName = qtProjectPathMock.getName()
        def deploymentSettingsFileMockPath = "${buildDir}/" +
                "android-$targetName-deployment-settings.json"
        return createMockFile(deploymentSettingsFileMockPath)
    }

    File createQtCMakeFileMock(QtBuildTask buildTask) {
        def fileEnd = buildTask.isWindows() ? '.bat' : ''
        def mockQtCMakePath = "${buildTask.qtKitDir}/bin/qt-cmake${fileEnd}"
        return createMockFile(mockQtCMakePath)
    }

    File createQtNinjaFileMock(QtBuildTask buildTask) {
        def fileEnd = buildTask.isWindows() ? '.exe' : ''
        def mockQtNinjaPath = "${buildTask.qtPath}/Tools/Ninja/ninja${fileEnd}"
        return createMockFile(mockQtNinjaPath)
    }

    File createLocalPropertiesFileMock() {
        def localPropertiesFile = createMockFile("${mockProject.projectDir}/local.properties")
        try {
            localPropertiesFile.text = new File('local.properties').text
        } catch (FileNotFoundException) {
            def sdkLocation = System.getenv("ANDROID_SDK_ROOT")
            assert sdkLocation : "Error: can't parse SDK location from local.properties or ANDROID_SDK_ROOT"
            localPropertiesFile.text = "sdk.dir=${sdkLocation}"
        }
        return localPropertiesFile
    }

    void cleanupMockFiles(File... files) {
        files.each { it.delete() }
    }
}
