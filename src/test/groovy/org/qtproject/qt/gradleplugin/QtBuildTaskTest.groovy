// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

package org.qtproject.qt.gradleplugin

import groovy.json.JsonException
import groovy.json.JsonSlurper
import org.gradle.api.GradleException
import spock.lang.Specification

class QtBuildTaskTest extends Specification {
    @Delegate
    QtGradlePluginTestHelper testHelper = new QtGradlePluginTestHelper()

    void setupSpec() {
        QtGradlePluginTestHelper.mockProjectAndSetExtensionProperties()
    }

    void 'test QtBuildTask gets properties from extension'() {
        when: 'extension is found and properties are set'
        def pluginExtension = getQtPluginExtensionFromProject()
        pluginExtension.qtKitDir = QtGradlePluginTestHelper.qtKitDirMock

        then: 'task properties are set correctly'
        def buildTask = getQtBuildTaskFromProject()
        buildTask.qtPath == pluginExtension.qtPath.path
        buildTask.qtKitDir == pluginExtension.qtKitDir.path
        buildTask.projectPath == pluginExtension.projectPath
    }

    void 'test QtBuildTask creates empty settings file'() {
        given: 'mock deploymentSettingsFile is created'
        def buildTask = getQtBuildTaskFromProject()
        def deploymentSettingsFileMock =
                createDeploymentSettingsFileMock(buildTask.getBuildDirectory())

        when: 'creating createEmptySettingsGradle is called'
        def emptySettingsGradle = buildTask.createEmptySettingsGradle()

        then: 'empty settings.gradle is created successfully'
        emptySettingsGradle.exists()

        cleanup: 'delete created mock deployment and settings files'
        cleanupMockFiles(emptySettingsGradle, deploymentSettingsFileMock)
    }

    void 'test QtBuildTask exec with failing input'() {
        given: 'project setup with buildTask is done'
        def buildTask = getQtBuildTaskFromProject()

        when: 'when given a failing command'
        def start = QtBuildTask.isWindows() ? ['cmd.exe', '/c'] : ['./gradlew']
        def failingCommand = start + ['wrong_argument']
        buildTask.exec(failingCommand)

        then: 'throws an GradleException and fails'
        thrown(GradleException)
    }

    void 'test QtBuildTask exec with valid input'() {
        given: 'project setup with buildTask is done'
        def buildTask = getQtBuildTaskFromProject()

        when: 'when given a valid command'
        def start = QtBuildTask.isWindows() ? ['cmd.exe', '/c'] : []
        def validCommand = start + ['echo', 'Testing testing']
        buildTask.exec(validCommand)

        then: 'should not throw an exception'
        noExceptionThrown()
    }

    void 'test QtBuildTask ReadJson from valid json file'() {
        given: 'path to valid json file'
        def validJsonFile = setupMockJsonFile('jsonMockValid', validMockJsonFile())

        when: 'reading json from file'
        def result = QtBuildTask.readJsonFromFile(validJsonFile.path)
        def apiVersion = 'Working!'
        def compilerId = 'Clang'

        then: 'result should contain Name and QDocModule'
        result.built_with.android.api_version == apiVersion
        result.built_with.compiler_id == compilerId

        cleanup: "delete created mock json"
        cleanupMockFiles(validJsonFile)
    }

    void 'test QtBuildTask ReadJson from non json file'() {
        given: 'path to invalid json file'
        def invalidJsonFile = setupMockJsonFile('jsonMockNotValid', invalidMockJsonFile())

        when: 'reading from file'
        QtBuildTask.readJsonFromFile(invalidJsonFile.path)

        then: 'a JsonException is throw'
        thrown(JsonException)

        cleanup: "delete created mock json"
        cleanupMockFiles(invalidJsonFile)
    }

    void 'test QtBuildTask ExtractValue from json'() {
        given: 'valid json key and data'
        def jsonKey = 'built_with.compiler_id'
        def jsonData = new JsonSlurper().parseText(validMockJsonFile())

        when: 'extracting the key value from given data'
        def jsonValue = QtBuildTask.extractValueFromJson(jsonData, jsonKey)

        then: 'gets the correct value from that key'
        def correctValue = 'Clang'
        jsonValue == correctValue
    }

    void 'test QtBuildTask fail to extract value from json'() {
        given: 'valid json file, data with wrong key'
        def jsonKey = 'DoesNotExist'
        def jsonData = new JsonSlurper().parseText(validMockJsonFile())

        when: 'extracting the key value from given data'
        def jsonValue = QtBuildTask.extractValueFromJson(jsonData, jsonKey)

        then: 'no key found'
        jsonValue == null
    }

    void 'test QtBuildTask getQtCMakeWrapperPath returns an existing path'() {
        given: "a mock qt-cmake file is created"
        def buildTask = getQtBuildTaskFromProject()
        buildTask.qtKitDir = new File("${buildTask.buildDirectory}" +
                "${QtGradlePluginTestHelper.qtKitDirMock}")
        def mockQtCMakeFile = createQtCMakeFileMock(buildTask)

        when: "getQtCMakeWrapperPath is called"
        def cmakeDir = buildTask.getQtCMakeWrapperPath()

        then: "qt-cmake file is found at the appropriate path"
        assert new File(cmakeDir).exists()

        cleanup: "delete created mock qt-cmake file"
        cleanupMockFiles(mockQtCMakeFile)
    }

    void 'test QtBuildTask ninja found at correct path'() {
        given: "a mock ninja file is created"
        def buildTask = getQtBuildTaskFromProject()
        def mockQtNinjaFile = createQtNinjaFileMock(buildTask)

        when: "getNinjaPath is called"
        buildTask.resolveNinjaPath()

        then: "ninja file is found at the appropriate path"
        assert new File(buildTask.qtNinjaPath).exists()

        cleanup: "delete created mock ninja file"
        cleanupMockFiles(mockQtNinjaFile)
    }

    void 'test QtBuildTask gets valid SDK path'() {
        when: 'getSDKDirectory is called'
        def buildTask = getQtBuildTaskFromProject()
        def sdkPath = buildTask.resolvedSdkDirectory()

        then: 'mocked SDK path exists'
        sdkPath != null
        new File(sdkPath).exists()
    }

    void 'test QtBuildTask with incorrect SDK path'() {
        given: 'mocked local.properties file includes a invalid sdk path'
        def localPropertiesFile = createLocalPropertiesFileMock()
        def pattern = ~'sdk.dir.*'
        localPropertiesFile.text = localPropertiesFile.text
                .replaceAll(pattern, 'sdk.dir=notvalidPath')

        when: 'wrong path found in local.properties'
        def buildTask = getQtBuildTaskFromProject()
        buildTask.resolvedSdkDirectory()

        then: 'GradleException is thrown'
        thrown(GradleException)

        cleanup: 'delete mock local properties file'
        cleanupMockFiles(localPropertiesFile)
    }

    void 'test QtBuildTask gets valid NDK path with 6.8.0 keys'() {
        given: 'project setup with qt kit dir is done'
        def buildTask = getQtBuildTaskFromProject()
        def buildDir = buildTask.buildDirectory.path
        buildTask.qtKitDir = new File("${buildDir}" +
                "${QtGradlePluginTestHelper.qtKitDirMock}")

        def coreMockFile = createMockFile("${buildTask.qtKitDir}/modules/Core.json")
        coreMockFile.text = coreJson_6_8()
        def coreJson = buildTask.readJsonFromFile(coreMockFile.path)
        def ndkVersion = buildTask.extractValueFromJson(coreJson, 'built_with.android.ndk.version')
        def ndkPath = createMockFile("${buildDir}/ndk/$ndkVersion")

        when: 'getNDKDirectory is called'
        def ndk = buildTask.getNDKDirectory(buildDir, ndkVersion)

        then: 'NDK directory is found'
        ndk != null
        new File(ndk).exists()

        cleanup: 'delete mock files'
        cleanupMockFiles(coreMockFile, ndkPath)
    }

    void 'test QtBuildTask gets valid NDK path with 6.9.0 keys'() {
        given: 'project setup with qt kit dir is done'
        def buildTask = getQtBuildTaskFromProject()
        def buildDir = buildTask.buildDirectory.path
        buildTask.qtKitDir = new File("${buildDir}" +
                "${QtGradlePluginTestHelper.qtKitDirMock}")

        def coreMockFile = createMockFile("${buildTask.qtKitDir}/modules/Core.json")
        coreMockFile.text = coreJson_6_9()
        def coreJson = buildTask.readJsonFromFile(coreMockFile.path)
        def ndkVersion = buildTask.extractValueFromJson(coreJson, 'platforms.targets.ndk_version')
        def ndkPath = createMockFile("${buildDir}/ndk/$ndkVersion")

        when: 'getNDKDirectory is called'
        def ndk = buildTask.getNDKDirectory(buildDir, ndkVersion)

        then: 'NDK directory is found'
        ndk != null
        new File(ndk).exists()

        cleanup: 'delete mock files'
        cleanupMockFiles(coreMockFile, ndkPath)
    }

    void 'test QtBuildTask exec with nonexistent command'() {
        given: 'project setup with buildTask is done'
        def buildTask = getQtBuildTaskFromProject()

        when: 'when given a nonexistent command'
        def badCommand = ['this_is_not', 'a', 'legit command']
        buildTask.exec(badCommand)

        then: 'throws a GradleException and fails'
        thrown(GradleException)
    }

    void 'test QtBuildTask add dependency to implementation configuration'() {
        given: 'project setup with project path is done'
        def project = QtGradlePluginTestHelper.mockProject

        and: 'mock deploymentSettingsFile is created'
        def buildTask = getQtBuildTaskFromProject()
        def deploymentSettingsFileMock =
                createDeploymentSettingsFileMock(buildTask.getBuildDirectory())

        and: 'java plugin is applied'
        project.plugins.apply('java')

        when: 'dependency is added'
        def addedDependency = buildTask.addDependency()

        then: 'project implementation config contains the added dependency'
        def implementationConfig = project.configurations.named('implementation').get()
        assert implementationConfig.dependencies.contains(addedDependency)

        cleanup: 'delete created mock deployment file'
        cleanupMockFiles(deploymentSettingsFileMock)
    }
}
